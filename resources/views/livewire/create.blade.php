<div class="Fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm-block sm:p-4">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>

        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog">
            <div>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="forName" class="block text-gray-700 text-sm font-bold mb-2">Nama:</label>
                            {{-- input.shadow.appearance-none.border.rounded.w-full.py-2 easy shortcut --}}
                            <input type="text" wire:model="name" class="shadow appearance-none border rounded w-full py-2 text-gray-700 leading-tight focus:outline:none focus:shadow-outlne" id="forName">
                            @error('name')
                                <span class="text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="forEmail" class="block text-gray-700 text-sm font-bold mb-2">Email:</label>
                            <input type="text" wire:model="email" class="shadow appearance-none border rounded w-full py-2 text-gray-700 leading-tight focus:outline:none focus:shadow-outlne" id="forEmail">
                            @error('email')
                                <span class="text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="forPhoneNumber" class="block text-gray-700 text-sm font-bold mb-2">Telp:</label>
                            <input type="text" wire:model="phone_number" class="shadow appearance-none border rounded w-full py-2 text-gray-700 leading-tight focus:outline:none focus:shadow-outlne" id="forPhoneNumber">
                            @error('phone_number')
                                <span class="text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="forStatus" class="block text-gray-700 text-sm font-bold mb-2">Status:</label>
                            <select wire:model="status" id="forStatus" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus-outline-none shadow-outline">
                                <option value="">Pilih</option>
                                <option value="1">Premium</option>
                                <option value="0">Free</option>
                            </select>
                            @error('status')
                                <span class="text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm-6 flex-row-reverse">
                    <span class="mt-3 flex w-full rounded-md shadow-sm ml-3 mt-0 w-auto">
                        <button wire:click.prevent="store()" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm focus-outline-none transition ease-in-out duration-150 text-sm leading-5">
                            save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm ml-3 mt-0 w-auto">
                        <button wire:click="closeModal()" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm focus-outline-none transition ease-in-out duration-150 text-sm leading-5">
                            cancel
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>